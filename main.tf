provider "aws" {

    region = "us-east-1"
}


variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "my_ip"{}
variable "instance_type"{}
variable "public_key_location"{}

# used to do variable interpolation
variable "env_prefix" {}


resource "aws_vpc" "myapp-vpc" {

    cidr_block = var.vpc_cidr_block

    tags = {
        Name = "${var.env_prefix}-vpc"
        
    }
}


resource "aws_subnet" "myapp-subnet-1"{    
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name =  "${var.env_prefix}-subnet-1" 
    }
    

}

#we are telling to which vpc belongs
#igw means internet gateway
resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.myapp-vpc.id 

    tags = {
        Name = "${var.env_prefix}-igw"
    }
}



#using the existing default/main routable 
#updates the default main route table with the values provided
resource "aws_default_route_table" "main-rtb" {
    default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name =  "${var.env_prefix}-main-rtb"
    }

}



#setting a security group
#ingress inboud
#egress outbound
resource "aws_default_security_group" "default-sg" {
  
    vpc_id = aws_vpc.myapp-vpc.id 

    # just the ips in var.my_ip can access port 22
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

    # any ip address can access port 8080
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    #allowing outgoing communication using any port, any protocol, any ip
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = [] #for allow accessing vpcs endpoints
    }

    tags = {
        Name =  "${var.env_prefix}-default-sg"
    }
}


#querying the AMI
data "aws_ami" "latest-amazon-linux-image"{
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}


#generating a keypair
resource "aws_key_pair" "ssh-key" {
    key_name = "server-key"
    #you can use the file location too
    # you can use no internpolation>    file(var.public_key_location)
    public_key = "${file(var.public_key_location)}"
}



#creating an EC2 instance
resource "aws_instance" "myapp-server" {
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_default_security_group.default-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name

    user_data = file("entry-script.sh")

    tags = {
        Name =  "${var.env_prefix}-server"
    }

}





# outputs values after the terraform apply is ran, the values belongs to the resources recently created
# you can do terraform plan in order to have a list of attributes to choose and use with output function
# you need to define one output for each attribute, you cant have two values in the same output
#you use the internal variable names
output "myapp-vpc-id" {
    value = aws_vpc.myapp-vpc.id 
}

output "myapp-subnet-1-id" {
    value = aws_subnet.myapp-subnet-1.id 
}

output "myapp-igw-id" {
    value = aws_internet_gateway.myapp-igw.id
}

output "aws_ami_id" {
    value = data.aws_ami.latest-amazon-linux-image.id
}

output "aws_instance_id" {
    value = aws_instance.myapp-server.id
}

output "ec2_public_ip" {
    value = aws_instance.myapp-server.public_ip 
}